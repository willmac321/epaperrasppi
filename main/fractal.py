import math

class Line:
    def __init__(self, x0, y0, x1, y1, w, angle, i, d, width, height):
        # d is depth count, index is the index for line within that depth layer
        self.x0, self.y0, self.x1, self.y1 = x0, y0, x1, y1
        self.angle = angle
        self.index = i
        self.d = d
        self.lineWidth = w
        self.width = width
        self.height = height

    def print(self):
        return "d: {}, i: {}, x0: {}, y0: {}, x1: {}, y1: {}, angle: {}"\
                .format(self.d, self.index, math.ceil(self.x0), math.ceil(self.y0), 
                        math.ceil(self.x1), math.ceil(self.y1), math.ceil(self.angle / math.pi * 180))

    @property
    def coords(self):
#        if self.x0 < 0:
#            self.x0 = 0
#        elif self.x0 > self.width:
#            self.x0 = self.width
#        if self.x1 < 0:
#            self.x1 = 0
#        elif self.x1 > self.width:
#            self.x1 = self.width
#        if self.y0 < 0:
#            self.y0 = 0
#        elif self.y0 > self.height:
#            self.y0 = self.height
#        if self.y1 < 0:
#            self.y1 = 0
#        elif self.y1 > self.height:
#            self.y1 = self.height
        return ((self.x0, self.y0), (self.x1, self.y1))
        #return ((round(self.x0), round(self.y0)), (round(self.x1), round(self.y1)))


class Fractal:
    def __init__(self, width, height, depth, logging):
        self.logging = logging
        self.width = width
        self._triangularNumber = depth
        self.res = (width) / self.triangularNumber[0]
        self.depth = depth

        self.arr = self.createArray(Line(width/2, 0, width/2, height - depth * self.res, 0, math.pi / 2, 0, depth, width, height), [])

    def createArray(self, line, arr):
        """ 
            logic:
                add each line to array, return arrays and then combine
                later on might try to figure out how to filter duplicate lines
        """
        i = line.index
        d = line.d

        if line in arr:
            self.logging.debug('base case achieved, lines in array')
            return arr

        arr.append(line) 

        if (i < self.depth):
            #create split in line to two lines - define new endpoints and angles
            w = line.lineWidth
            new_x1 = line.x1 + d * math.cos(line.angle) * self.res
            new_y1 = line.height - d * math.sin(line.angle) * self.res
            new_angle1 = line.angle + (i * self.res) / 180 * math.pi
            new_angle2 = line.angle - (i * self.res) / 180 * math.pi

            # increase index in depth layer
            d -= 1
            i += 1
            arr1 = self.createArray(Line(line.x1, line.y1, new_x1, new_y1, w, new_angle1, i, d, line.width, line.height), arr)
            arr2 = self.createArray(Line(line.x1, line.y1, new_x1, new_y1, w, new_angle2, i, d, line.width, line.height), arr1)
#            if(isinstance(arr1, list)):
#                for a in arr1:
#                    self.logging.debug(a.print())
#            else:
#                self.logging.debug(type(arr1))
#                self.logging.debug(arr1)
#            if(isinstance(arr2, list)):
#                for a in arr2:
#                    self.logging.debug(a.print())
#            else:
#                self.logging.debug(type(arr2))
#                self.logging.debug(arr2)

            if(isinstance(arr1, list) and isinstance(arr2, list)):
                self.logging.debug("both lists")
                return arr2
        elif len(arr) > 0:
            self.logging.debug('base case achieved, array len: {}'.format(len(arr)))
            return arr
        else:
            self.logging.debug('return empty array')
            return []

    @property
    def triangularNumber(self):
        """Get the triangular number and base in a tuple"""
        return self._triangularNumber, (math.sqrt(self._triangularNumber * 8 + 1) - 1) / 2

    @triangularNumber.setter
    def triangularNumber(self, n):
        self._triangularNumber =  (n * (n + 1)) / 2

    def factorial(self, num):
        if(num>0):
            return num * self.factorial(num-1)
        else:
            return num

    
