
## Class to hold ePaper methods and params and Draw things to epaper display ##
## Also have listeners for button presses on epaper ##

import sys
import os
picdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'pic')
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)
from waveshare_epd import epd2in7
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import RPi.GPIO as GPIO
import logging
import enum

class Fonts:
    font14 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 14)
    font24 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)

class Buttons(enum.Enum):
    # these are the pins attached to keys
    # board is using BCM input 
    key1 = 5
    key2 = 6
    key3 = 13
    key4 = 19

class EPaper:
    def __init__(self, logging, callback1):

        self.logging = logging


        self.logging.info("init ePaper display")
        self.epd = epd2in7.EPD()
        self.epd.init()
        self.makeNewImage('L')
        self.fonts = Fonts()
        self.attachButtonEvents(callback1)
        self.width = self.epd.width
        self.height = self.epd.height
    #    epd.Clear(0xFF)

    def attachButtonEvents(self, callback1):
        for k in (Buttons):
            GPIO.setup(k.value, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        GPIO.add_event_detect(Buttons.key1.value, GPIO.RISING, callback=self.clearAll)
        GPIO.add_event_detect(Buttons.key2.value, GPIO.RISING)
        GPIO.add_event_detect(Buttons.key3.value, GPIO.RISING)
        GPIO.add_event_detect(Buttons.key4.value, GPIO.RISING, callback=callback1)


    def makeNewImage(self, val = 'L'):
        ## 1 is 1 bit display, L is 8-bit
        # 255: clear the frame
        # used to clear image buffer 
        self.Himage = Image.new(val, (self.epd.width, self.epd.height), 255)  
        self.draw = ImageDraw.Draw(self.Himage)

    def drawFromBuffer(self):
        self.epd.display(self.epd.getbuffer(self.Himage))
        time.sleep(2)

    def clearAll(self, chan = 5):
        # clear image buffer and ePaper screen
        self.logging.info('Clear E-Paper display')
        self.makeNewImage
        self.epd.Clear(0xFF)
        time.sleep(2)

    def drawFromImg(self, file):
        with Image.open('../test.jpg') as f:
            f = f.rotate(90)
            
            f = f.resize((self.epd.width, self.epd.height))
            self.Himage.paste(f, (0,0))
        self.drawFromBuffer()


    def writeSingleLineText(self, x=0, y=0, text='test, no text supplied', font = None):
        # if font is null set default
        if not font:
            font = self.fonts.font14
        self.logging.info('Write single line of text:\n'+str(text))
        # write a single line text
        self.draw.text((x, y), text, font = font, fill = 0)
        self.drawFromBuffer()

    def drawFromArray(self, arr):
        # use line class from fractal.py
        # x0, y0, x1, y1, width, angle, _, _
        self.logging.info('Drawing from array')
        for i in arr:
        #    self.logging.debug(i.print())
            self.draw.line(i.coords, fill = 0)

        self.drawFromBuffer()
